CREATE TABLE [Country] (
	id integer NOT NULL,
	name varchar(50) NOT NULL,
  CONSTRAINT [PK_COUNTRY] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [City] (
	id integer NOT NULL,
	name varchar(50) NOT NULL,
  CONSTRAINT [PK_CITY] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [Aeroport] (
	id integer NOT NULL,
	name varchar(50) NOT NULL,
  CONSTRAINT [PK_AEROPORT] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [Planes] (
	id integer NOT NULL,
	name varchar(50) NOT NULL,
	seats_id integer NOT NULL,
  CONSTRAINT [PK_PLANES] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [Flyes] (
	id integer NOT NULL,
	name varchar(50) NOT NULL,
	plane_id integer NOT NULL,
	from_id integer NOT NULL,
	to_id integer NOT NULL,
  CONSTRAINT [PK_FLYES] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [Aviacompany] (
	id integer NOT NULL,
	name varchar(50) NOT NULL,
  CONSTRAINT [PK_AVIACOMPANY] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [Tickets] (
	id integer NOT NULL,
	flyes_id integer NOT NULL,
  CONSTRAINT [PK_TICKETS] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [Seats_all] (
	id integer NOT NULL,
	size integer NOT NULL,
	category_id integer NOT NULL,
	num_first integer NOT NULL,
	num_last integer NOT NULL,
  CONSTRAINT [PK_SEATS_ALL] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [Category] (
	id integer NOT NULL,
	name varchar NOT NULL,
  CONSTRAINT [PK_CATEGORY] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [Plains_all] (

)
GO



ALTER TABLE [Planes] WITH CHECK ADD CONSTRAINT [Planes_fk0] FOREIGN KEY ([seats_id]) REFERENCES [Seats_all]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [Planes] CHECK CONSTRAINT [Planes_fk0]
GO




ALTER TABLE [Seats_all] WITH CHECK ADD CONSTRAINT [Seats_all_fk0] FOREIGN KEY ([category_id]) REFERENCES [Category]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [Seats_all] CHECK CONSTRAINT [Seats_all_fk0]
GO



